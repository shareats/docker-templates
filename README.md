# SharEats Docker Templates

## Développement

Placer le dossier docker à côté du dossier racine du projet

```tree

└─── <projet>
│
└─── docker
        │ docker-compose.yml
        │ Dockerfile
        │ .env
        │ <.m2>
        │ <mysql>
```

Editer le fichier .env

```env
SERVICE_NAME = Nom du container que l'on va lancer (ex : auth-spring)
PROJECT_NAME = Nom du dossier contenant le projet
DB_NAME = Nom de la base de données
USER_NAME = User de la BD MySQL
USER_PWD = Mot de passe du user
ROOT_PWD = Mot de passe du user "root" de MySQL
XXX_PORT = Port de la machine hôte sur lequel lancer le processus associé
```

Ensuite il suffit de lancer le script avec ```docker-compose up``` et un dossier .m2 va se créer contenant les dépendances du projet.
**Ces dossiers sont des volumes pour la persistance des données et éviter de télécharger les dépendances pour Maven et de perdre sa base de données MySQL à chaque lancement du container**
